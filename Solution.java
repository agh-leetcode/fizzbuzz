
import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        int n = 15;
        fizzBuzz(n);
    }

    private List<String> fizzBuzz(int n) {
        List<String> result = new ArrayList<>();
        StringBuilder builder = new StringBuilder();

        for (int i = 1; i <= n; i++) {
            builder.setLength(0);
            if (i%3==0){
                builder.append("Fizz");
            }
            if (i%5==0){
                builder.append("Buzz");
            } else if(builder.length()==0){
                builder.append(String.valueOf(i));
            }
            result.add(builder.toString());
        }

        return result;
    }

    private List<String> fizzBuzz2(int n) {
        List<String> result = new ArrayList<>();
        for(int i=1; i<=n; i++) {
            result.add(convert(i));
        }
        return result;
    }

    private String convert(int n) {
        if(n%15 == 0) return "FizzBuzz";
        else if(n%5 == 0) return "Buzz";
        else if(n%3 == 0) return "Fizz";
        else
            return String.valueOf(n);
    }

    private List<String> fizzBuzz3(int n) {
        List<String> result = new ArrayList<>(n);
        for(int i=1,fizz=0,buzz=0;i<=n ;i++){
            fizz++;
            buzz++;
            if(fizz==3 && buzz==5){
                result.add("FizzBuzz");
                fizz=0;
                buzz=0;
            }else if(fizz==3){
                result.add("Fizz");
                fizz=0;
            }else if(buzz==5){
                result.add("Buzz");
                buzz=0;
            }else{
                result.add(String.valueOf(i));
            }
        }
        return result;
    }
}
